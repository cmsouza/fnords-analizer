<?php

namespace Fnords\Matchers;

/*
#apache
"(\w{3}) (\d{2}) (\d{2}:\d{2}:\d{2}) (\d{4})
(\d{2})-(\w{3})-(\d{4}) (\d{2}:\d{2}:\d{2}) ([\w/_]+)
PHP (\w+): (.*) in (/srv/[\w\/\.]+) on line (\d+)(.*)
*/

class ApacheMatcher extends Matcher
{
	public function getDate()
	{

	}

	public function getServer()
	{
		return $this->input['server'];
	}

	public function getApplication()
	{
		//não mudar a ordem
		$regexes["@webmaster.corp.folha.com.br/admin/(\w+)/@"] = "__match__";
		$regexes["@(cpndin.com.br)@"] = "__match__";
		$regexes["@(\w+).corp.folha.com.br@"] = "__match__";
		$regexes["@(\w+).folha.com.br@"] = "__match__";
		$regexes["@(\w+).folha.uol.com.br@"] = "__match__";
		$regexes["@(\w+).folha.com@"] = "__match__";
		$regexes["@common/classes/circulation/(\w+)/@"] = "__match__";
		$regexes["@common/classes/(\w+)/@"] = "__match__";
		$regexes["@common/includes/(\w+)/@"] = "__match__";
		$regexes["@common/classes/([\w_\.]+.php)@"] = "spiffy_framework";
		$regexes["@prod_bin/(\w+)/@"] = "__match__";
		$regexes["@test_bin/(\w+)/@"] = "__match__";
		$regexes["@prod_bin/(\w+.php)@"] = "Unknown";
		$regexes["@prod_data/(\w+)/@"] = "__match__";
		$regexes["@(pagseguro)//job/transacao@"] = "livraria";
		$regexes["@Error reading from remote server returned by /(.*)$@"] = "Unknown";
		$regexes["@/etc/(php.ini)@"] = "__match__";
		$regexes["@/www.(\w+).com.br@"] = "__match__";
		$regexes["@/home/(\w+)/@"] = "Unknown";
		$regexes["@(simplexml_load_string)\(\): Entity@"] = "Unknown";
		$regexes["@PHP Deprecated:  Directive '(register_globals)'@"] = "php.ini";
		$regexes["@Unable to load dynamic library '(/usr/local/php5/.*?.so)'@"] = "php.ini";
		$regexes["@http://(api.somarmeteorologia.com.br)/@"] = "weather";
		$regexes["@Unknown: Server (\d{1,3}\.\d{1,3}.\d{1,3}.\d{1,3}) \(tcp 11211\)@"] = "Unknown";
		$regexes["@(failed to open stream: HTTP request failed)! HTTP/1.0 500 Internal Server Error@"] = "Unknown";
		$regexes["@(https://nfe.fazenda.sp.gov.br/cteWEB/)@"] = "moss";
		$regexes["@(https://mdfe.sefaz.rs.gov.br/ws)@"] = "moss";
		$regexes["@(ASN1_CHECK_TLEN):wrong tag@"] = "Unknown";
		$regexes["@(ASN1_ITEM_EX_D2I):nested asn1 error@"] = "Unknown";
		$regexes["@(Unknown: Key cannot be empty in)@"] = "Unknown";
		$regexes["@(Unknown: Input variables exceeded \d+)@"] = "Unknown";
		$regexes["@(PHP Parse error:  syntax error)@"] = "Unknown";
		$regexes["@(expects parameter 1 to be resource)@"] = "Unknown";

		foreach($regexes as $regex => $value) {
			preg_match($regex, $this->input['error'], $matches);
			if (!empty($matches)) break;

		}
		$result = $value == "__match__" ? $matches[1] : $value ;
		
		if (!$matches || !$matches[1]) {
			print $this->input['error'];
			die();
		}

		return $result;
	}

	public function getType()
	{

	}

	public function getMessage()
	{

	}

}