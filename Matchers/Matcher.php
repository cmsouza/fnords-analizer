<?php

namespace Fnords\Matchers;

abstract class Matcher
{
	public static function createMatcher($input)
	{
		if ($input['source'] == 'spiffy_event') {
			return new SpiffyMatcher($input);
		}

		if ($input['source'] == 'apache') {
			return new ApacheMatcher($input);
		}
	}

	public function __construct($input)
	{
		$this->input = $input;
	}
}