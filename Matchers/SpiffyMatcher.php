<?php

namespace Fnords\Matchers;

/*
Date/Time: \w{3}, (\d{2}) (\w{3}) (\d{4}) (\d{2}:\d{2}:\d{2})
Service: ([\w\s]+)/([\w\s]+) \((\w+)\)
Host: (.*?) \((.*?)\)
Notification Type: (\w+)
Message: (.*?)$
*/

class SpiffyMatcher extends Matcher
{
	public function getDate()
	{

	}

	public function getServer()
	{
		$regex = '/Host: (.*?) \((.*?)\)/';
		$matches = [];

		preg_match($regex, $this->input['error'],$matches);
		return sprintf("%s(%s)", $matches[1], $matches[2]);
	}

	public function getApplication()
	{

	}

	public function getType()
	{

	}

	public function getMessage()
	{

	}

}