<?php

namespace Fnords;

require('vendor/autoload.php');

use PDO;
use Dotenv;
use Fnords\Matchers\Matcher;

Dotenv::load(__DIR__);

$dbName = getenv('DB_NAME');
$host = getenv('DB_HOST');
$port = getenv('DB_PORT');
$user = getenv('DB_USER');
$password = getenv('DB_PASSWORD');

$conn_string = sprintf('mysql:dbname=%s;host=%s:%d',$dbName, $host, $port);
$conn = new PDO($conn_string, $user, $password);

$sql = "SELECT * from fnords";
$sth = $conn->prepare($sql);
$sth->execute();

$count = 0;
while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
	if ($row['source'] == 'spiffy_event') continue;
	$m = Matcher::createMatcher($row);
	print $m->getApplication() . "\n";
}

print "\n$count\n";